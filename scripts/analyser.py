#python 2.7
#Author: James Parker

import os
import string
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import subjectivity
from nltk.sentiment import SentimentAnalyzer
from nltk.sentiment.util import *
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk import tokenize


def getPrintable (s):
  printable = set(string.printable)
  return filter(lambda x: x in printable, s)

def show_downloader ():
  nltk.download()

def train ():
  n_instances = 100
  subj_docs = [(sent, 'subj') for sent in subjectivity.sents(categories='subj')[:n_instances]]
  obj_docs = [(sent, 'obj') for sent in subjectivity.sents(categories='obj')[:n_instances]]
  
  train_subj_docs = subj_docs[:80]
  test_subj_docs = subj_docs[80:100]
  train_obj_docs = obj_docs[:80]
  test_obj_docs = obj_docs[80:100]

  training_docs = train_subj_docs+train_obj_docs
  testing_docs = test_subj_docs+test_obj_docs
  sentim_analyzer = SentimentAnalyzer()
  all_words_neg = sentim_analyzer.all_words([mark_negation(doc) for doc in training_docs])
  unigram_feats = sentim_analyzer.unigram_word_feats(all_words_neg, min_freq=4)

  sentim_analyzer.add_feat_extractor(extract_unigram_feats, unigrams=unigram_feats)
  training_set = sentim_analyzer.apply_features(training_docs)
  test_set = sentim_analyzer.apply_features(testing_docs)

  trainer = NaiveBayesClassifier.train
  classifier = sentim_analyzer.train(trainer, training_set)

  sid = SentimentIntensityAnalyzer()

  # for sentence in sentences:
  #   print(sentence)
  #   ss = sid.polarity_scores (sentence)
  #   for k in sorted (ss):
  #     print ('{0}: {1}, '.format(k, ss[k])),
  #   print ()

  path = "../txt/"
  filecount = 0
  a = 1
  for filename in os.listdir (path):
    if a <= 3000:
      a +=1
      continue
    filecount += 1;
    filepath = path + filename
    

    with open(filepath, 'r') as file_content:
      sentences = []
      file_text = file_content.read()
      try:
        lines_list = tokenize.sent_tokenize(file_text)
        sentences.extend(lines_list)

        sentence_count = 0
        neg = 0
        neu = 0
        pos = 0
        compound = 0

        for sentence in sentences:
          ss = sid.polarity_scores (sentence)
          if ss['neu'] != 1:
            neg += ss['neg']  
            neu += ss['neu']
            pos += ss['pos']
            compound += ss['compound']
            sentence_count += 1
        if sentence_count != 0:
          neg /= sentence_count
          neu /= sentence_count
          pos /= sentence_count
          compound /= sentence_count

        print filename
        # print ('neg: {0}, neu: {1}, pos: {2}, compound: {3}'.format (neg, neu, pos, compound))
        print (neg, neu, pos, compound)
      except Exception as e:

        file_text = getPrintable (file_text)
        sentences.extend(lines_list)

        sentence_count = 0
        neg = 0
        neu = 0
        pos = 0
        compound = 0

        for sentence in sentences:
          ss = sid.polarity_scores (sentence)
          if ss['neu'] != 1:
            neg += ss['neg']  
            neu += ss['neu']
            pos += ss['pos']
            compound += ss['compound']
            sentence_count += 1

        neg /= sentence_count
        neu /= sentence_count
        pos /= sentence_count
        compound /= sentence_count
        print filename
        # print ('neg: {0}, neu: {1}, pos: {2}, compound: {3}'.format (neg, neu, pos, compound))
        print (neg, neu, pos, compound)
train ()



# show_downloader ()