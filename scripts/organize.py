import json
import unicodedata
path_to_frequency = "../ANU_Innovation/MostFrequentWordsNEW.txt"
path_to_semantic  = "sematic_analysis.txt" 
path_to_age       = "output_2.txt"
json_file         = "v1.json"
json_file_2       = "v2.json"
json_file_3       = "v3.json"
json_file_4       = "v4.json"

def combine ():
  with open(path_to_frequency, 'r') as f, open(path_to_semantic, 'r') as s:
    data = json.load(f)
    count = 0
    while True:
      title    = s.readline().replace('\n', '')
      if not title: break
      neg      = float (s.readline())
      neu      = float (s.readline())
      pos      = float (s.readline())
      compound = float (s.readline())

      for book in data["books"]:
        filename = book["filename"]
        # filename.encode('ascii','ignore')
        # print type (filename), type (title)
        # print filename
        # print repr(title.decode('unicode-escape'))
        if filename.encode ("utf-8")  == title:
          semantic = {}
          semantic["neg"] = neg
          semantic["neu"] = neu
          semantic["pos"] = pos
          semantic["compound"] = compound
          book["semantic"] = semantic
          count += 1
          print count
          break

    jsonData = json.dumps(data)

  print(jsonData)
  with open(json_file, 'w') as f:
       json.dump(data, f)

def comb_r ():
  with open(json_file, 'r') as j, open(path_to_age, 'r') as a:
    data = json.load(j)
    data_age = json.loads(unicode (a.read(), "ISO-8859-1"))
    count = 0

    for book_age in data_age["books"]:
      book_name = book_age["title"]
      difficulty = book_age["d"]
      for book in data["books"]:
        filename = book["filename"]
        # print title
        # print filename.encode ("utf-8")
        if book_name.encode ("utf-8") in filename.encode ("utf-8"):
          book["reading_dif"] = difficulty
          count += 1
          print count
          break

    jsonData = json.dumps(data)

  print(jsonData)
  with open(json_file_2, 'w') as f:
       json.dump(data, f)

def get_min_max ():
  min_v = 1
  max_v = -1
  with open(json_file_2, 'r') as j:
    data = json.load(j)
    for book in data["books"]:
      neg = book["semantic"]["neg"]
      if not neg: 
        continue
      pos = book["semantic"]["pos"]
      diff = pos - neg
      if diff > max_v:
        max_v = diff
      if diff < min_v:
        min_v = diff

  print max_v, min_v

def add_semantic_index (max_v, min_v):
  with open(json_file_2, 'r') as j:
    data = json.load(j)
    for book in data["books"]:
      neg = book["semantic"]["neg"]
      if not neg:
        continue
      pos = book["semantic"]["pos"]
      diff = pos - neg
      index =  (2 * (diff - min_v) / (max_v - min_v)) - 1
      book["semantic"]["index"] = index

  with open(json_file_3, 'w') as j:
     json.dump(data, j)

def add_author_title ():
  with open(json_file_3, 'r') as j:
    data = json.load(j)
    count = 0
    for book in data["books"]:
      filename = book["filename"]
      separator = filename.find ("___")
      txt      = filename.find (".txt")
      book["id"] = count
      count += 1
      author = filename[:separator]
      title = filename[separator + 3:txt]
      book["author"] = author
      book["title"] = title

  with open(json_file_4, 'w') as j:
     json.dump(data, j)

add_author_title ()