class MinHeap {
    int size;
    int capacity;
    MyNode[] nodes;
}

class MyNode {
    String word;
    int frequency;

    //This is extra pointer to point to Trie
    MyTrieNode node;
}