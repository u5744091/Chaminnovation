import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class TrieForOccurenceOfString {
    public static void main(String[] args) throws IOException {
        String[] filenames = {
                "Abraham Lincoln___Lincoln Letters.txt", "Abraham Lincoln___Lincoln's First Inaugural Address.txt",
                "Abraham Lincoln___Lincoln's Gettysburg Address, given November 19, 1863.txt",
                "Abraham Lincoln___Lincoln's Inaugurals, Addresses and Letters (Selections).txt",
                "Abraham Lincoln___Lincoln's Second Inaugural Address.txt",
                "Abraham Lincoln___Speeches and Letters of Abraham Lincoln, 1832-1865.txt",
                "Abraham Lincoln___State of the Union Addresses.txt", "Abraham Lincoln___The Emancipation Proclamation.txt",
                "Abraham Lincoln___The Life and Public Service of General Zachary Taylor_ An Address.txt",
                "Abraham Lincoln___The Writings of Abraham Lincoln, Volume 1_ 1832-1843.txt",
                "Abraham Lincoln___The Writings of Abraham Lincoln, Volume 2_ 1843-1858.txt",
                "Abraham Lincoln___The Writings of Abraham Lincoln, Volume 3.txt",
                "Abraham Lincoln___The Writings of Abraham Lincoln, Volume 4.txt",
                "Abraham Lincoln___The Writings of Abraham Lincoln, Volume 5_ 1858-1862.txt",
                "Abraham Lincoln___The Writings of Abraham Lincoln, Volume 6_ 1862-1863.txt",
                "Abraham Lincoln___The Writings of Abraham Lincoln, Volume 7_ 1863-1865.txt",
                "Agatha Christie___The Mysterious Affair at Styles.txt", "Agatha Christie___The Secret Adversary.txt",
                "Albert Einstein___Relativity_ The Special and General Theory.txt",
                "Albert Einstein___Sidelights on Relativity.txt", "Aldous Huxley___Crome Yellow.txt", "Aldous Huxley___Mortal Coils.txt",
                "Aldous Huxley___The Defeat of Youth and Other Poems.txt", "Alexander Pope___An Essay on Criticism.txt",
                "Alexander Pope___Essay on Man.txt", "Alexander Pope___The Poetical Works of Alexander Pope, Volume 1.txt",
                "Alexander Pope___The Poetical Works of Alexander Pope, Volume 2.txt",
                "Alexander Pope___The Rape of the Lock and Other Poems.txt",
                "Alexander Pope___The Works of Alexander Pope, Volume 1.txt",
                "Alfred Russel Wallace___Contributions to the Theory of Natural Selection.txt",
                "Alfred Russel Wallace___Darwinism.txt", "Alfred Russel Wallace___Is Mars Habitable_.txt",
                "Alfred Russel Wallace___Island Life.txt", "Alfred Russel Wallace___The Malay Archipelago, Volume 1.txt",
                "Alfred Russel Wallace___The Malay Archipelago, Volume 2.txt"
        };
        String[] top100words = {"the","be","to","of","and","a","in","that","have","I","it","for","not","on","with","he","as","you","do","at","this",
                "but","his","by","from","they","we","say","her","she","or","an","will","my","one","all","would","there","their","what",
                "so","up","out","if","about","who","get","which","go","me","when","make","can","like","time","no","just","him","know",
                "take","people","into","year","your","good","some","could","them","see","other","than","then","now","look","only","come",
                "its","over","think","also","back","after","use","two","how","our","work","first","well","way","even","new","want","because",
                "any","these","give","day","most","us"};
        String[] top500words = {"i", "and", "the", "you", "uh", "to", "a", "that", "it", "of", "yeah", "know", "in", "like", "they", "have", "so",
                "was", "but", "is", "it's", "we", "huh", "just", "oh", "do", "don't", "that's", "well", "for", "what", "on", "think",
                "right", "not", "um", "or", "my", "be", "really", "with", "he", "one", "are", "this", "there", "I'm", "all", "if", "no",
                "get", "about", "at", "out", "had", "then", "because", "go", "up", "she", "when", "them", "can", "would", "as", "me",
                "mean", "some", "good", "got", "OK", "people", "now", "going", "were", "lot", "your", "time", "see", "how", "they're",
                "kind", "here", "from", "did", "something", "too", "more", "very", "want", "little", "been", "things", "an", "you're",
                "said", "there's", "I've", "much", "where", "two", "thing", "her", "didn't", "other", "say", "back", "could", "their",
                "our", "guess", "yes", "way", "has", "down", "we're", "any", "he's", "work", "take", "even", "those", "over", "probably",
                "him", "who", "put", "years", "sure", "can't", "pretty", "gonna", "stuff", "come", "these", "by", "into", "went", "make",
                "than", "year", "three", "which", "home", "will", "nice", "never", "only", "his", "doing", "cause", "off", "I'll",
                "maybe", "real", "why", "big", "actually", "she's", "day", "five", "always", "school", "look", "still", "around",
                "anything", "kids", "first", "does", "need", "us", "should", "talking", "last", "thought", "doesn't", "different",
                "money", "long", "used", "getting", "same", "four", "every", "new", "everything", "many", "before", "though", "most",
                "tell", "being", "bit", "house", "also", "use", "through", "feel", "course", "what's", "old", "done", "sort", "great",
                "bad", "we've", "another", "car", "true", "whole", "whatever", "twenty", "after", "ever", "find", "care", "better",
                "hard", "haven't", "trying", "give", "I'd", "problem", "else", "remember", "might", "again", "pay", "try", "place",
                "part", "let", "keep", "children", "anyway", "came", "six", "family", "wasn't", "talk", "made", "hundred", "night",
                "call", "saying", "dollars", "live", "away", "either", "read", "having", "far", "watch", "week", "mhm", "quite",
                "enough", "next", "couple", "own", "wouldn't", "ten", "interesting", "am", "sometimes", "bye", "seems", "heard",
                "goes", "called", "point", "ago", "while", "fact", "once", "seen", "wanted", "isn't", "start", "high", "somebody",
                "let's", "times", "guy", "area", "fun", "they've", "you've", "started", "job", "says", "play", "usually", "wow",
                "exactly", "took", "few", "child", "thirty", "buy", "person", "working", "half", "looking", "someone", "coming",
                "eight", "love", "everybody", "able", "we'll", "life", "may", "both", "type", "end", "least", "told", "saw", "college",
                "ones", "almost", "since", "days", "couldn't", "gets", "guys", "god", "country", "wait", "yet", "believe", "thinking",
                "funny", "state", "until", "husband", "idea", "name", "seven", "together", "each", "hear", "help", "nothing", "parents",
                "room", "today", "makes", "stay", "mom", "sounds", "change", "understand", "such", "gone", "system", "comes", "thank",
                "show", "thousand", "left", "friends", "class", "already", "eat", "small", "boy", "paper", "world", "best", "water",
                "myself", "run", "they'll", "won't", "movie", "cool", "news", "number", "man", "basically", "nine", "enjoy", "bought",
                "whether", "especially", "taking", "sit", "book", "fifty", "months", "women", "month", "found", "side", "food", "looks",
                "summer", "hmm", "fine", "hey", "student", "agree", "mother", "problems", "city", "second", "definitely", "spend", "happened",
                "hours", "war", "matter", "supposed", "worked", "company", "friend", "set", "minutes", "morning", "between", "music", "close",
                "leave", "wife", "knew", "pick", "important", "ask", "hour", "deal", "mine", "reason", "credit", "dog", "group", "turn",
                "making", "American", "weeks", "certain", "less", "must", "dad", "during", "lived", "forty", "air", "government", "eighty",
                "wonderful", "seem", "wrong", "young", "places", "girl", "happen", "sorry", "living", "drive", "outside", "bring", "easy",
                "stop", "percent", "hand", "gosh", "top", "cut", "computer", "tried", "gotten", "mind", "business", "anybody", "takes",
                "aren't", "question", "rather", "twelve", "phone", "program", "without", "moved", "gave", "yep", "case", "looked", "certainly",
                "talked", "beautiful", "card", "walk", "married", "anymore", "you'll", "middle", "tax"};
        File file;
        String[] top500wordsReduced = new String[top500words.length];
        for (int i = 0; i < top500words.length ; i++) {
            top500wordsReduced[i] = top500words[i].toLowerCase().replaceAll("[^A-Za-z0-9]", "");
        }
        System.out.println("{\"books\" : [");
        for (String filename : Filenames.filenameList) {
            file = new File("C:\\Users\\Chamin\\Projects\\ANU_Innovation\\src\\txt\\"+filename);
            BufferedReader reader = new BufferedReader(new FileReader(file));
            int k=5;
            MyTrie t = new MyTrie(k);
            String ss=null;
            while((ss=reader.readLine())!=null){
                String[] array=ss.split(" ");
                for (int i = 0; i < array.length; i++) {
                    if (!Arrays.asList(top500wordsReduced).contains(array[i].toLowerCase().replaceAll("[^A-Za-z0-9]", ""))) {
                        t.insert(array[i]);
                    }
                }
            }
            reader.close();

            System.out.print("{\"filename\", \"" + filename + "\", \"5 most frequent words\" : [");
            t.display();
            System.out.println("]}, ");
        }
        System.out.println("]}");
    }
} 